import Head from 'next/head'
import styles from '../styles/Home.module.css'
import axios from "axios";
import { useEffect, useState } from 'react';

export default function Home() {

  const ISSERVER = typeof window === "undefined";
  const [user, setUser] = useState('User')
  const [loggedIn, setIsLoggedIn] = useState(false);

  const registerUser = async event => {
    console.log(process.env.API_ENDPOINT);
    event.preventDefault()
    axios.post(process.env.API_ENDPOINT+`/admin-login`, {})
    .then((response) => {
      setUser('John Doe')
      setIsLoggedIn(true)
      localStorage.setItem("token", response.data.data.token);
    })
  }

  const redirectURL = () => {
    const token = localStorage.getItem("token")
    window.open('https://admin-authentication.vercel.app/auth/admin/'+token, '_blank');
  }


  useEffect(() => {
    if(!ISSERVER) {
      if(localStorage.getItem("token")) {
        setUser('John Doe')
        setIsLoggedIn(true)
      }
    }
  })

  return (
    <div className={styles.container}>
      <Head>
        <title>Redirect App</title>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        { !loggedIn ? 
          <div className={styles.grid}>
            <button onClick={registerUser} className={styles.card}>Login</button>
          </div>
          :
          <div>
            <button onClick={redirectURL} className={styles.card}>Go to other panel</button>
          </div>
        } 
        <div className={styles.grid}>
          <p>Welcome {user}</p>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
